package websocketIO

import (
	"encoding/json"
	"github.com/gorilla/websocket"
	"github.com/samisagit/go-im-server/src/channelHelper"
	"github.com/samisagit/go-im-server/src/message"
	"net/http"
)

// Configure the upgrader
var Upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func ReadWebSocketMessageToJSON(ws *websocket.Conn) (error, message.Message) {
	var msg message.Message
	err := ws.ReadJSON(&msg)
	return err, msg
}

func WriteRedisMessageToWebsocket(messageSlice []byte, ws *websocket.Conn) error {
	var msg message.Message
	json.Unmarshal(messageSlice, &msg)
	err := ws.WriteJSON(msg)
	return err
}

func RelayWsMessages(ws *websocket.Conn, kc chan string, messages chan message.Message) {
	for {
		// Read in a new message as JSON and map it to a Message type
		err, wsMsg := ReadWebSocketMessageToJSON(ws)
		if err != nil {
			if !channelHelper.IsChannelClosed(kc) {
				kc <- "HASH"
			}
			return
		}
		messages <- wsMsg
	}
}
