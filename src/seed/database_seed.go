package main

import (
	"github.com/samisagit/go-im-server/src/database"
	"log"
)

func seedUsers() int {
	userId, err := database.CreateUser(
		"sam",
		"test",
		"sam@example.com")
	if err != nil {
		log.Fatal(err)
	}
	return userId
}

func seedChannel() int {
	channelId, err := database.CreateChannel()
	if err != nil {
		log.Fatal(err)
	}
	return channelId
}

func main() {
	userId := seedUsers()
	channelId := seedChannel()
	database.CreateMessage(
		channelId,
		userId,
		"Welcome to your new chat")
}
