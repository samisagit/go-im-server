package message

type Message struct {
	ChatId  int    `json:"chatId"`
	UserId  int    `json:"userId"`
	Message string `json:"message"`
	Date    int    `json:"date"`
}
