package database

import (
	"testing"
)

func TestPingDb(t *testing.T) {
	err, db := DbInterface()
	if err != nil {
		t.Error(err)
	}
	defer db.Close()
	err = db.Ping()
	if err != nil {
		t.Error(err)
	}
}
