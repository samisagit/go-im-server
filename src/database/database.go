package database

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/samisagit/go-im-server/src/config"
)

type databaseConfig struct {
	host     string
	port     int
	user     string
	password string
	database string
}

func DbInterface() (error, *sql.DB) {
	dataBaseCreds := databaseConfig{
		config.Host,
		config.Port,
		config.User,
		config.Password,
		config.Database,
	}
	dataSource := dataBaseCreds.getDataSource()
	db, err := sql.Open("mysql", dataSource)
	return err, db
}

func (dbc databaseConfig) getDataSource() string {
	return dbc.user + ":" + dbc.password + "@tcp(" + dbc.host + ")/" + dbc.database
}

func CreateUser(name string, password string, email string) (int, error) {
	id := 0
	err, db := DbInterface()
	if err != nil {
		return id, err
	}
	defer db.Close()

	queryString := `
		INSERT INTO users (subscribedChannels, userName, password, email)
		VALUES (?, ?, ?, ?);`
	res, err := db.Exec(queryString, "{\"active\":[1]}", name, password, email)
	if err != nil {
		return id, err
	} else {
		id, err := res.LastInsertId()
		if err != nil {
			return int(id), err
		}
		return int(id), err
	}
}

func CreateChannel() (int, error) {
	id := 0
	err, db := DbInterface()
	if err != nil {
		return int(id), err
	}
	defer db.Close()

	queryString := `
		INSERT INTO channels (active) VALUES (true)`
	res, err := db.Exec(queryString)
	if err != nil {
		return id, err
	} else {
		id, err := res.LastInsertId()
		if err != nil {
			return int(id), err
		}
	}
	return int(id), err
}

func CreateMessage(channel int, sender int, content string) (int, error) {
	id := 0
	err, db := DbInterface()
	if err != nil {
		return id, err
	}
	defer db.Close()

	queryString := `
		INSERT INTO messages (channel, userId, content)
		VALUES (?, ?, ?);`
	_, err = db.Exec(queryString,
		channel,
		sender,
		content)
	if err != nil {
		return id, err
	}
	return id, err
}
