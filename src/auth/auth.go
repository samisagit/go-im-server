package auth

import (
	"encoding/json"
	"log"
	"net/http"
)

type AuthRequest struct {
	Username string `json:"name"`
	Password string `json:"password"`
}

func GetUsersChannels(r *http.Request) []string {
	value := make([]string, 0)
	// this will be a MySQL lookup for authorized channels
	value = append(value, "1", "2")
	return value
}

func ConfirmAuth(r AuthRequest) bool {
	return true
}

func HandleAuth(w http.ResponseWriter, r *http.Request) {
	// Pull the request into struct
	decoder := json.NewDecoder(r.Body)
	var ar AuthRequest
	err := decoder.Decode(&ar)
	if err != nil {
		log.Print(err)
	}
	authed := ConfirmAuth(ar)
	if authed {
		// This needs to be the JWT - then we're good
		messageBlob, err := json.Marshal(ar)
		if err != nil {
			log.Print(err)
		}
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(messageBlob))
		return
	}
	w.WriteHeader(http.StatusNotFound)
}
