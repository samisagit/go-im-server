package config

// Clone this file and name the clone config.go then uncomment and adjust the below
// settings to configure the workload of each redis sender in the new file.

// Set up max queued messages
//var MaxMessages = float64(10)

// Set up max redis senders
//var MaxSender = float64(1)

// Set up messages per sender count
//var SenderRatio = MaxSender / MaxMessages

// False stops GC entirely
//var AllowGC = false
