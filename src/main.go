package main

import (
	"github.com/gomodule/redigo/redis"
	"github.com/samisagit/go-im-server/src/auth"
	"github.com/samisagit/go-im-server/src/config"
	"github.com/samisagit/go-im-server/src/message"
	"github.com/samisagit/go-im-server/src/redisIO"
	"github.com/samisagit/go-im-server/src/websocketIO"
	"log"
	"net/http"
)

// Set up message channel
var messages = make(chan message.Message, int(config.MaxMessages))

func handleConnections(w http.ResponseWriter, r *http.Request) {
	// Upgrade initial GET request to a websocket
	ws, err := websocketIO.Upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
	}
	defer ws.Close()

	// Set up redis for this user
	redisChannels := auth.GetUsersChannels(r)
	c, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		log.Println(err)
		return
	}
	defer c.Close()
	psc := redis.PubSubConn{c}
	defer psc.Close()
	for i := 0; i < len(redisChannels); i++ {
		psc.Subscribe(redisChannels[i])
	}

	// Set up a kill switch for when a user leaves
	killChannel := make(chan string)
	go redisIO.RelayRedisMessages(ws, &psc, killChannel)
	go websocketIO.RelayWsMessages(ws, killChannel, messages)

	for {
		_ = <-killChannel
		close(killChannel)
		break
	}
}

func main() {
	// Configure routes
	http.HandleFunc("/ws", handleConnections)
	http.HandleFunc("/auth", auth.HandleAuth)

	// Redis messaging set up
	go redisIO.RedisWriteHandler(messages)

	// Start the server on localhost port 7070
	err := http.ListenAndServe(":7070", nil)
	if err != nil {
		log.Fatal(err)
	}
}
