package redisIO

import (
	"github.com/samisagit/go-im-server/src/config"
	"github.com/samisagit/go-im-server/src/message"
	"strconv"
	"testing"
	"time"
)

func BenchmarkRedisAtScale(b *testing.B) {
	var messages = make(chan message.Message, int(config.MaxMessages))
	for i := 1; i < cap(messages)+1; i++ {
		s := strconv.Itoa(i)
		messageContent := "test message " + s
		messages <- message.Message{
			1,
			2,
			messageContent,
			3,
		}
	}

	go RedisWriteHandler(messages)

	for {
		time.Sleep(100 * time.Millisecond)
		if len(messages) < 1 {
			return
		}
	}
}
