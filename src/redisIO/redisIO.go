package redisIO

import (
	"encoding/json"
	"fmt"
	"github.com/gomodule/redigo/redis"
	"github.com/gorilla/websocket"
	"github.com/samisagit/go-im-server/src/channelHelper"
	"github.com/samisagit/go-im-server/src/config"
	"github.com/samisagit/go-im-server/src/message"
	"github.com/samisagit/go-im-server/src/websocketIO"
	"log"
	"math"
	"strconv"
	"sync"
)

func newPool() *redis.Pool {
	return &redis.Pool{
		MaxIdle:   80,
		MaxActive: 1000, // max number of connections
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", ":6379")
			if err != nil {
				panic(err.Error())
			}
			return c, err
		},
	}

}

var pool = newPool()

func RelayRedisMessages(ws *websocket.Conn, psc *redis.PubSubConn, kc chan string) {
	for {
		switch v := psc.Receive().(type) {
		case redis.Message:
			// Write message to websocket
			err := websocketIO.WriteRedisMessageToWebsocket(v.Data, ws)
			if err != nil {
				if !channelHelper.IsChannelClosed(kc) {
					log.Println(err)
				}
				break
			}
		case error:
			if !channelHelper.IsChannelClosed(kc) {
				log.Println(v)
				kc <- "HASH"
			}
			return
		}
	}
}

func RedisWriteHandler(messageChannel chan message.Message) {
	senderCount := 0
	var m sync.Mutex
	c := sync.NewCond(&m)
	killswitch := make(chan string)

	for {
		length := float64(len(messageChannel))
		neededSenders := int(math.Ceil(length * config.SenderRatio))
		if senderCount < neededSenders || senderCount < 1 {
			log.Printf("Increasing sender count to %d, need %d", senderCount+1, neededSenders)
			go addRedisSender(messageChannel, killswitch, c)
			senderCount++
		} else if senderCount > neededSenders && senderCount > 1 {
			log.Printf("Decreasing sender count to %d, need %d", senderCount-1, neededSenders)
			killMessage := fmt.Sprintf("only need %d senders", neededSenders)
			killswitch <- killMessage
			senderCount--
		}
		// Only update if we've sent a message and there is a change either neededSenders or senderCount
		c.L.Lock()
		if (senderCount == 1 && neededSenders == 0) || senderCount == neededSenders {
			c.Wait()
		}
		c.L.Unlock()
	}
	log.Fatal("The redis handler unexpectedly went away")
}

func addRedisSender(messageChannel chan message.Message, killswitch chan string, cond *sync.Cond) {
	c := pool.Get()
	defer c.Close()
Loop:
	for {
		select {
		case _ = <-killswitch:
			break Loop
		case msg := <-messageChannel:
			cond.Signal()
			redisChannel := strconv.Itoa(msg.ChatId)
			messageBlob, err := json.Marshal(msg)
			if err != nil {
				log.Println(err)
			}
			_, err = c.Do("PUBLISH", redisChannel, messageBlob)
			if err != nil {
				log.Println(err)
			}
		}
	}
	log.Println("Closed redis sender")
}
